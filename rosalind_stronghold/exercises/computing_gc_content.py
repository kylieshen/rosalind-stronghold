# encoding: utf-8
u'''
Problem

The GC-content of a DNA string is given by the percentage of symbols in the
string that are 'C' or 'G'. For example, the GC-content of "AGCTATAG" is 37.5%.
Note that the reverse complement of any DNA string has the same GC-content.

DNA strings must be labeled when they are consolidated into a database. A
commonly used method of string labeling is called FASTA format. In this format,
the string is introduced by a line that begins with '>', followed by some
labeling information. Subsequent lines contain the string itself; the first line
to begin with '>' indicates the label of the next string.

In Rosalind's implementation, a string in FASTA format will be labeled by the ID
"Rosalind_xxxx", where "xxxx" denotes a four-digit code between 0000 and 9999.

*Given*: At most 10 DNA strings in FASTA format (of length at most 1 kbp each).

*Return*: The ID of the string having the highest GC-content, followed by the
GC-content of that string. Rosalind allows for a default error of 0.001 in all
decimal answers unless otherwise stated; please see the note on absolute error
below.

Note on Absolute Error

We say that a number x is within an absolute error of y to a correct solution
if x is within y of the correct solution. For example, if an exact solution is
6.157892, then for x to be within an absolute error of 0.001, we must have that
|x−6.157892|<0.001, or 6.156892<x<6.158892.

Error bounding is a vital practical tool because of the inherent round-off error
in representing decimals in a computer, where only a finite number of decimal
places are allotted to any number. After being compounded over a number of
operations, this round-off error can become evident. As a result, rather than
testing whether two numbers are equal with x=z, you may wish to simply verify
that |x−z| is very small.

The mathematical field of numerical analysis is devoted to rigorously studying
the nature of computational approximation.

'''
name = 'Computing GC Content'
ID = 'GC'
order = 5

from ..util import random_dna_sequence, stderr
import random
import sys

def generate() :
  num_seqs = random.randint(2,10)
  recs = []
  for i in range(num_seqs) :
    seq_id = random.randint(1000,9999)
    recs.append('>Rosalind_{}\n'.format(seq_id))
    recs.append(random_dna_sequence()+'\n')
  return (''.join(recs),)

def run(fasta_str) :
  fasta_itr = iter(fasta_str.strip().split('\n'))
  max_name, max_gc = None, 0
  for header in fasta_itr :
    seq = next(fasta_itr)
    gc = 1.*(seq.count('G')+seq.count('C'))/len(seq)
    if gc > max_gc :
      max_name = header[1:]
      max_gc = gc
  print(max_name)
  print(max_gc*100)

tmpl_str = '''\
def run(fasta_str) :
  # calculate the %GC content of each DNA sequence in the fasta records of fasta_str
  # fasta_str is a single string with each line separated by a newline character
  # retain the sequence name and gc content of the sequence with the highest %gc
  # print out the sequence name with the print function on its ownline
  # then print out the %GC of the sequence with the print function on the next line
'''
def template() :
  return tmpl_str

def check(user_output, expected_output) :
  # output should have exactly two lines of text
  # the first line is the name of the sequence with the greatest %GC
  # the second line is the %GC
  def parse_output(output) :
    lines = output.split('\n')
    lines = [_ for _ in lines if len(_.strip()) != 0]
    return lines

  user_output = parse_output(user_output)
  expected_output = parse_output(expected_output)

  if len(user_output) != len(expected_output) :
    stderr('user output does not have the correct number of lines')
    return False

  if user_output[0] != expected_output[0] :
    stderr('user output seq name does not match expected')
    return False

  try :
    user_gc = float(user_output[1])
    expected_gc = float(expected_output[1])
  except :
    stderr('gc content line does not seem to contain a float value')
    return False

  return abs(user_gc-expected_gc)<0.001
