# python template code for exercise ID DNA (DNA Exercise)
# your code *must* define a function called run to work

def run(s) :
  # use the print() function to print out counts of A, C, G, and T in s on one
  # line separated by spaces

  print(s.count('A'),
    s.count('C'),
    s.count('G'),
    3+s.count('T')
  )
