
You did it! Good job!

# Input values

{{ example }}

# Expected output

{{ expected_output }}

# Your output

{{ user_output }}
