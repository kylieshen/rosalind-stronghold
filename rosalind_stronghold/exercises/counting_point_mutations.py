# encoding: utf-8
'''
[Rosalind URL](http://rosalind.info/problems/hamm/)
Problem

Given two strings s and tt of equal length, the Hamming distance between s and
t, denoted d_H(s,t), is the number of corresponding symbols that differ in s
and t.

*Given*: Two DNA strings s and t of equal length (not exceeding 1 kbp).

*Return*: The Hamming distance dH(s,t)dH(s,t).

Sample Dataset

GAGCCTACTAACGGGAT
CATCGTAATGACGGCCT

Sample Output

7

'''
name = 'Counting Point Mutations'
ID = 'HAMM'
order = 6

from ..util import random_dna_sequence
import random

def generate() :
  length=random.randint(10,1000)
  return (random_dna_sequence(length,length),random_dna_sequence(length,length))

def run(s,t) :
  dist = 0
  for s_i,t_i in zip(s,t) :
    if s_i != t_i :
      dist += 1
  print(dist)

tmpl_str = '''\
def run(s,t) :
  # count the number of base positions that do not have the same nucleotide
  # across DNA sequences s and t
  # print out the number of differing positions with the print function
'''
def template() :
  return tmpl_str
