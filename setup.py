#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='rosalind-stronghold',
      version='1.0'
      ,description='Command line translation of the Rosalind Bioinformatics Stronghold exercises'
      ,author='Adam Labadorf'
      ,author_email='labadorf@bu.edu'
      ,install_requires=[
        'docopt'
        ,'terminaltables'
        ,'fabulous'
        ,'pytest'
        ,'jinja2'
        ,'future'
      ]
      ,packages=find_packages()
      ,package_data={'rosalind_stronghold': ['templates/*.md']}
      ,entry_points={
        'console_scripts': [
          'rosalind=rosalind_stronghold.rosalind:main'
        ]
      }
      ,setup_requires=['pytest-runner']
      ,tests_require=['pytest']
     )
