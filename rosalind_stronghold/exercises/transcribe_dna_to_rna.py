'''

[Rosalind URL](http://rosalind.info/problems/rna/)

Problem

An RNA string is a string formed from the alphabet containing 'A', 'C', 'G',
and 'U'.

Given a DNA string t corresponding to a coding strand, its transcribed RNA
string uu is formed by replacing all occurrences of 'T' in t with 'U' in uu.

*Given*: A DNA string t having length at most 1000 nt.

*Return*: The transcribed RNA string of t.
'''
name = 'Transcribing DNA into RNA'
ID = 'RNA'
order = 1

from ..util import random_dna_sequence

def generate() :
  return(random_dna_sequence(),)

def run(t) :
  print(t.replace('T','U'))

tmpl_str = '''\
def run(t) :
  # print out the DNA sequence in t replacing all Ts with Us
'''
def template() :
  return tmpl_str
